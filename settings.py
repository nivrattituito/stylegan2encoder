import os

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

#-------------------------------------------------------
# models folder
#-------------------------------------------------------
MODELS_DIR = os.path.join(
    BASE_DIR, 'resources/models'
)
os.makedirs(MODELS_DIR, exist_ok=True)


#-------------------------------------------------------
# media folder
# images/ result files etc
#-------------------------------------------------------
MEDIA_DIR = os.path.join(
    BASE_DIR, 'media'
)
os.makedirs(MEDIA_DIR, exist_ok=True)

#-------------------------------------------------------
# result folder
#-------------------------------------------------------
RESULT_DIR = os.path.join(
    MEDIA_DIR, 'results'
)
os.makedirs(RESULT_DIR, exist_ok=True)
