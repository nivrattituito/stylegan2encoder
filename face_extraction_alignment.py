"""
Usage:
    1) terminal:
        python face_extraction_alignment.py -r "/raw_images_dir" -a "/aligned_images"
"""
import os
import sys
from face.face_alignment import image_align
from face.landmarks_detector import LandmarksDetector

from loguru import logger

from helper import download_drive_file
from settings import MODELS_DIR, MEDIA_DIR

# global landmark detector variable
landmarks_model_path = os.path.join(MODELS_DIR, "shape_predictor_68_face_landmarks.dat")
if not os.path.exists(landmarks_model_path):
    download_drive_file(file_id="1lHlWxCIhMLhq06EtCA-dGOHTdGqfSiGD", output=landmarks_model_path)

landmarks_detector = LandmarksDetector(landmarks_model_path)


def single_image_extract_align_face(raw_image, aligned_face_path=None):
    """
    Extract and align single image face

    Args:
        raw_image (str | ndarray): raw image path or image in numpy array
        aligned_face_path (str, optional): extracted and aligned face path. Defaults to None.
    """
    from nutils.basic_file_folder_creation import generate_filename

    if aligned_face_path:
        if not os.path.exists(os.path.dirname(aligned_face_path)):
            os.makedirs(os.path.dirname(aligned_face_path))

        # checks if path is a directory
        if os.path.isdir(aligned_face_path):
            if isinstance(raw_image, str):
                img_name = os.path.basename(raw_image)
                face_img_name = '%s.png' % (os.path.splitext(img_name)[0])
        else:
            face_img_name = None # none if already passed

    aligned_faces = []
    for i, face_landmarks in enumerate(landmarks_detector.get_landmarks(raw_image), start=1):

        params = {
            'image': raw_image,
            'face_landmarks': face_landmarks,
        }
        if aligned_face_path:
            if face_img_name:
                face_img_name = f"{i}_{face_img_name}"
                aligned_face_path = os.path.join(aligned_face_path, face_img_name)

            params["dst_file"] = aligned_face_path
        else:
            params["save_out_image"] = False
            params["return_np"] = True
            
        aligned_face = image_align(**params)
        aligned_faces.append(aligned_face)
    return aligned_faces


def extract_align_face_fulldir(raw_images_dir, aligned_images_dir):
    """
    Extracts and aligns all faces from images using DLib. Process all images of directory
    """
    lst_image_names = [f for f in os.listdir(raw_images_dir) if f[0] not in '._']

    for img_name in lst_image_names:
        raw_img_path = os.path.join(raw_images_dir, img_name)
        
        for i, face_landmarks in enumerate(landmarks_detector.get_landmarks(raw_img_path), start=1):
            face_img_name = '%s_%02d.png' % (os.path.splitext(img_name)[0], i)
            aligned_face_path = os.path.join(aligned_images_dir, face_img_name)
            os.makedirs(aligned_images_dir, exist_ok=True)
            image_align(raw_img_path, face_landmarks, dst_file=aligned_face_path)

    total_processed = len(lst_image_names)
    logger.info(f"Processed total {total_processed} images.")
    return total_processed

def main():
    import argparse
    parser = argparse.ArgumentParser(description = "command line parser")
    parser.add_argument("-r", "--raw", help = "Raw images dir", required = True, default = "")
    parser.add_argument("-a", "--aligned", help = "Aligned images dir", required = True, default = "")

    argument = parser.parse_args()

    raw_images_dir = argument.raw
    aligned_images_dir = argument.aligned

    # start
    extract_align_face_fulldir(raw_images_dir, aligned_images_dir)

if __name__ == "__main__":
    main()
