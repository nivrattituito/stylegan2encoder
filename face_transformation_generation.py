"""
Utilities to modify age of face and generate new images of younger or older age 
with diffrent emotions and face angles.
"""
import os
import math
import pickle

import numpy as np
from PIL import Image
import tensorflow as tf

import dnnlib
import dnnlib.tflib as tflib

#**Declare the necessary functions**
def plot_two_images(img1,img2, img_id, fs = 12):
    import matplotlib.pyplot as plt
    f, axarr = plt.subplots(1,2, figsize=(fs,fs))
    axarr[0].imshow(img1)
    axarr[0].title.set_text('Encoded img %d' %img_id)
    axarr[1].imshow(img2)
    axarr[1].title.set_text('Original img %d' %img_id)
    plt.setp(plt.gcf().get_axes(), xticks=[], yticks=[])
    plt.show()

def display_sbs(folder1, folder2, res = 256):
    if folder1[-1] != '/': folder1 += '/'
    if folder2[-1] != '/': folder2 += '/'

    imgs1 = sorted([f for f in os.listdir(folder1) if '.png' in f])
    imgs2 = sorted([f for f in os.listdir(folder2) if '.png' in f])
    if len(imgs1)!=len(imgs2):
        print("Found different amount of images in aligned vs raw image directories. That's not supposed to happen...")

    for i in range(len(imgs1)):
        img1 = Image.open(folder1 + imgs1[i]).resize((res,res))
        img2 = Image.open(folder2 + imgs2[i]).resize((res,res))
        plot_two_images(img1,img2, i)
        print("")

def load_generator(network='default', model=None, other_networks=[], networks_urls={}):
    """
    load stylegan generator

    Args:
        network (str, optional): Stylegan network. Defaults to 'default'. network = 'default' #@param ["default", "european", "asian", "asian beauty", "baby"]
        model (str, optional): Stylegan2 model name if network default is selected. Defaults to None.
        other_networks (list, optional): list of network names. Defaults to [].
        networks_urls (dict, optional): dict of . Defaults to {}.
    """
    if network == 'default':
        network_pkl = 'networks/' + model
    else:
        # other_networks = !ls networks/other
        if networks_urls[network][1] in other_networks:
            network_pkl = 'networks/other/' + networks_urls[network][1]
        else:
            network_url = networks_urls[network][0]
            try:
                logger.info(f"downloading file from google drive. Path: {network_url}")
                output_folder = os.path.join('networks/other/', network_name)
                download(network_url, output_folder, quiet=False)

                network_name = networks_urls[network][1]
                network_pkl = 'networks/other/' + network_name
            except BaseException:
                network_pkl = 'networks/other/' + networks_urls[network][1]

    tflib.init_tf()
    with open(network_pkl, "rb") as f:
        generator_network, discriminator_network, Gs_network = pickle.load(f)

    w_avg = Gs_network.get_var('dlatent_avg')
    noise_vars = [var for name, var in Gs_network.components.synthesis.vars.items() if name.startswith('noise')]
    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = 1
    truncation_psi = 0.5
    return Gs_syn_kwargs

# configure generator
def configure_generator(Gs_network):
    """
    Configure stylegan generator
    """
    w_avg = Gs_network.get_var('dlatent_avg')
    noise_vars = [var for name, var in Gs_network.components.synthesis.vars.items() if name.startswith('noise')]
    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = 1
    truncation_psi = 0.5
    return Gs_syn_kwargs

def move_latent_and_save(latent_vector, direction_file, coeffs, Gs_network, Gs_syn_kwargs):
    # direction = np.load('latent_directions/' + direction_file)
    direction = np.load('latent_directions/' + direction_file)
    os.makedirs('results/'+direction_file.split('.')[0], exist_ok=True)
    
    for i, coeff in enumerate(coeffs):
        new_latent_vector = latent_vector.copy()
        new_latent_vector[0][:8] = (latent_vector[0] + coeff*direction)[:8]
        images = Gs_network.components.synthesis.run(new_latent_vector, **Gs_syn_kwargs)
        result = PIL.Image.fromarray(images[0], 'RGB')
        result.thumbnail(size, PIL.Image.ANTIALIAS)
        result.save('results/'+direction_file.split('.')[0]+'/'+str(i).zfill(3)+'.png')
        if len(coeffs)==1:
          return result


def generate_photo_animation(latent_file, parameter, intensity, boost_intensity=False, resolution="512"):
    """
    generate photo animation by modifying single feature

    Args:
        latent_file (str): Latent vector file
        parameter (str): feature to modify. ex. ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
        intensity (float): value to modify feature
        boost_intensity (bool, optional): If you set the boost_intensity, then the intensity will increase 3 times.. Defaults to False.
        resolution (str, optional): Resolution. Defaults to "512". values are [128, 256, 512, 1024]
    """
    from edit_photo import move_latent_and_save
    v = np.load(latent_file)
    v = np.array([v])

    direction_file = parameter + '.npy'
    direction_name = direction_file.split('.')[0]

    if boost_intensity == True:
	    intensity *= 3
	    coeffs = [intensity]

    size = int(resolution), int(resolution)

    move_latent_and_save(
        v, direction_file, coeffs, Gs_network, Gs_syn_kwargs
    )

def move_latent_and_save_multi_param(
    latent_vector, direction_intensity, frame_num, Gs_network, Gs_syn_kwargs, size=(1024, 1024),
    out_dir="results/3param", img_prefix="", img_name="result", img_postfix="", img_ext=".png",
    ):
    os.makedirs(out_dir, exist_ok=True)

    new_latent_vector = latent_vector.copy()
    new_latent_vector[0][:8] = (latent_vector[0] + direction_intensity)[:8]
    images = Gs_network.components.synthesis.run(new_latent_vector, **Gs_syn_kwargs)
    result = Image.fromarray(images[0], 'RGB')
    # result.thumbnail(size, Image.ANTIALIAS)

    out_imgname = img_prefix +  img_name + "_" + str(frame_num) + img_postfix + img_ext
    # print(out_imgname)
    # result.save('results/3param/result_(' + str(frame_num+1000) + ')_3param.png')
    result.save(os.path.join(out_dir, out_imgname))
    return (result, out_imgname)

def generated_manipulated_face_photos(latent_vector, tasks, dir_latent_directions, out_dir, Gs_network, Gs_syn_kwargs, size=(1024, 1024), global_fixed_feature_value={}, video=False, display_video=False):
    """
    Generate multiple photos by manipulating latent face

    manipulation factors:
      age, gface angle, emotions etc.
    """
    valid_latent_parameters = [
        "age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust",
        "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise",
        "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender",
        "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio",
        "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"
    ]
    total_generated_frames = 0

    for idx, task in enumerate(tasks, start=1):
        # print(idx)
        # print(task)
        number_of_frames = task.get("number_of_frames")
        # print(number_of_frames)

        total_generated_frames += number_of_frames

        manipulation = task.get("manipulation")
        # print(manipulation)

        lst_coeffs = []
        lst_direction_files = []
        out_img_name = ""

        for m in manipulation:
            parameter = m.get("parameter")
            intensity = m.get("intensity")
            boost_intensity = m.get("boost_intensity")
            locked_feature = m.get("locked_feature")

            print(parameter)

            if parameter not in valid_latent_parameters:
                logger.error(f"Invalid latent parameter '{parameter}'.")
                continue

            if intensity == 0:
                intensity += 0.001

            direction_file = np.load(
                os.path.join(
                    dir_latent_directions,
                    parameter + '.npy'
                )
            )
            lst_direction_files.append(direction_file)

            coeffs = []
            for i in range(0, number_of_frames):
                if parameter in global_fixed_feature_value:
                    intensity = global_fixed_feature_value.get(parameter)
                    coeffs.append(intensity)
                else:
                    if locked_feature:
                        coeffs.append(intensity)
                    else:
                        coeffs.append(
                            round((i * intensity) / number_of_frames, 3)
                        )
                        # print(round((i*intensity1)/number_of_frames,3))
            
            print(coeffs)
            lst_coeffs.append(coeffs)
            
            ## output image name generation
            if out_img_name:
                out_img_name += "__"
            out_img_name += parameter
            # add operator and value in str format 
            str_operator = "_plus" if intensity > 0 else  "_minus"
            out_img_name +=  str_operator + str(abs(intensity))

        print(f"Length of lst_coeffs: {len(lst_coeffs)}")
        print(f"Length of lst_direction_files: {len(lst_direction_files)}")
        # print(f"out_img_name: {out_img_name}")
        
        lst_imgfile_names = []
        for i in range(0, number_of_frames):
            direction_intensity = 0.001
            for c, coeffs in enumerate(lst_coeffs):
                direction_intensity += lst_direction_files[c] * coeffs[i]
        
            _r, out_imgname = move_latent_and_save_multi_param(
                latent_vector, direction_intensity, i, Gs_network, Gs_syn_kwargs,
                out_dir=out_dir, img_name=out_img_name,
            )
            lst_imgfile_names.append(out_imgname)
            # print(f"out_imgname: {out_imgname}")

        if video:
            img_dir = out_dir
            out_videofilename = out_imgname + ".mp4"
            out_videofile = generate_video_animation(
                img_dir, lst_imgfile_names,
                out_videofilename=out_videofilename
            )

        # display
        if display_video:
            from moviepy import editor as mpy
            display(mpy.ipython_display(out_videofile, height=400, autoplay=1, loop=1))

    return total_generated_frames