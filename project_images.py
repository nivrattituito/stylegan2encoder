import argparse
import os
import shutil
import numpy as np

import dnnlib
import dnnlib.tflib as tflib
import pretrained_networks
import projector
import dataset_tool
from training import dataset
from training import misc

from loguru import logger

def project_image(base_dir, proj, src_file, video=False):
    projector_dir = os.path.join(base_dir, "projector")
    tmp_dir = os.path.join(projector_dir, ".stylegan2-tmp")
    os.makedirs(tmp_dir, exist_ok=True)
    data_dir = '%s/dataset' % tmp_dir
    if os.path.exists(data_dir):
        shutil.rmtree(data_dir)

    image_dir = '%s/images' % data_dir
    tfrecord_dir = '%s/tfrecords' % data_dir
    os.makedirs(image_dir, exist_ok=True)

    shutil.copy(src_file, image_dir + '/')
    dataset_tool.create_from_images(tfrecord_dir, image_dir, shuffle=0)
    dataset_obj = dataset.load_dataset(
        data_dir=data_dir, tfrecord_dir='tfrecords',
        max_label_size=0, repeat=False, shuffle_mb=0
    )

    logger.info('Projecting image "%s"...' % os.path.basename(src_file))
    images, _labels = dataset_obj.get_minibatch_np(1)
    images = misc.adjust_dynamic_range(images, [0, 255], [-1, 1])
    proj.start(images)
    if video:
        video_dir = '%s/video' % tmp_dir
        os.makedirs(video_dir, exist_ok=True)
    while proj.get_cur_step() < proj.num_steps:
        print('\r%d / %d ... ' % (proj.get_cur_step(), proj.num_steps), end='', flush=True)
        proj.step()
        if video:
            filename = '%s/%08d.png' % (video_dir, proj.get_cur_step())
            misc.save_image_grid(proj.get_images(), filename, drange=[-1,1])
    print('\r%-30s\r' % '', end='', flush=True)

    dst_dir = os.path.join(projector_dir, "generated_images")
    os.makedirs(dst_dir, exist_ok=True)
    generated_image_file = os.path.join(dst_dir, os.path.basename(src_file)[:-4] + '.png')
    misc.save_image_grid(proj.get_images(), generated_image_file, drange=[-1,1])
    
    latent_dst_dir = os.path.join(projector_dir, "latent_representations")
    os.makedirs(latent_dst_dir, exist_ok=True)
    latent_file = os.path.join(latent_dst_dir, os.path.basename(src_file)[:-4] + '.npy')
    np.save(latent_file, proj.get_dlatents()[0])
    return (generated_image_file, latent_file)

def render_video(src_file, dst_dir, tmp_dir, num_frames, mode, size, fps, codec, bitrate):

    import PIL.Image
    import moviepy.editor

    def render_frame(t):
        frame = np.clip(np.ceil(t * fps), 1, num_frames)
        image = PIL.Image.open('%s/video/%08d.png' % (tmp_dir, frame))
        if mode == 1:
            canvas = image
        else:
            canvas = PIL.Image.new('RGB', (2 * src_size, src_size))
            canvas.paste(src_image, (0, 0))
            canvas.paste(image, (src_size, 0))
        if size != src_size:
            canvas = canvas.resize((mode * size, size), PIL.Image.LANCZOS)
        return np.array(canvas)

    src_image = PIL.Image.open(src_file)
    src_size = src_image.size[1]
    duration = num_frames / fps
    filename = os.path.join(dst_dir, os.path.basename(src_file)[:-4] + '.mp4')
    video_clip = moviepy.editor.VideoClip(render_frame, duration=duration)
    video_clip.write_videofile(filename, fps=fps, codec=codec, bitrate=bitrate)


def project_single_images_interface(
    src_file, dst_dir, tmp_dir=".stylegan2-tmp", 
    network_pkl='gdrive:networks/stylegan2-ffhq-config-f.pkl',
    vgg16_pkl='https://drive.google.com/uc?id=1N2-m9qszOeVC9Tq77WxsLnuWwOedQiD2',
    num_steps=1000, initial_learning_rate=0.1, initial_noise_factor=0.05,
    verbose=False, video=False, video_mode=1, video_size=1024, 
    video_fps=25, video_codec='libx264', video_bitrate='5M'
    ):
    """
    Project real-world images into StyleGAN2 latent space

    Args:
        src_file (str): aligned image for projection
        dst_dir (str): Output directory
        tmp_dir (str, optional): Temporary directory for tfrecords and video frames. Defaults to ".stylegan2-tmp".
        network_pkl (str, optional): StyleGAN2 network pickle filename. Defaults to 'gdrive:networks/stylegan2-ffhq-config-f.pkl'.
        vgg16_pkl (str, optional): VGG16 network pickle filename. Defaults to 'https://drive.google.com/uc?id=1N2-m9qszOeVC9Tq77WxsLnuWwOedQiD2'.
        num_steps (int, optional): Number of optimization steps. Defaults to 1000.
        initial_learning_rate (float, optional): Initial learning rate. Defaults to 0.1.
        initial_noise_factor (float, optional): Initial noise factor. Defaults to 0.05.
        verbose (bool, optional): Verbose output. Defaults to False.
        video (bool, optional): Render video of the optimization process. Defaults to False.
        video_mode (int, optional): Video mode: 1 for optimization only, 2 for source + optimization. Defaults to 1.
        video_size (int, optional): Video size (height in px). Defaults to 1024.
        video_fps (int, optional): Video framerate. Defaults to 25.
        video_codec (str, optional): Video codec. Defaults to 'libx264'.
        video_bitrate (str, optional): Video bitrate. Defaults to '5M'.
    """
    logger.info('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    proj = projector.Projector(
        vgg16_pkl             = vgg16_pkl,
        num_steps             = num_steps,
        initial_learning_rate = initial_learning_rate,
        initial_noise_factor  = initial_noise_factor,
        verbose               = verbose
    )
    proj.set_network(Gs)

    project_image(proj, src_file, dst_dir, tmp_dir, video=video)
    if video:
        render_video(
            src_file, dst_dir, tmp_dir, num_steps, video_mode,
            video_size, video_fps, video_codec, video_bitrate
        )
    shutil.rmtree(tmp_dir)


def project_images_fulldir_interface(
    src_dir, dst_dir, tmp_dir=".stylegan2-tmp", 
    network_pkl='gdrive:networks/stylegan2-ffhq-config-f.pkl',
    vgg16_pkl='https://drive.google.com/uc?id=1N2-m9qszOeVC9Tq77WxsLnuWwOedQiD2',
    num_steps=1000, initial_learning_rate=0.1, initial_noise_factor=0.05,
    verbose=False, video=False, video_mode=1, video_size=1024, 
    video_fps=25, video_codec='libx264', video_bitrate='5M'
    ):
    """
    Project real-world images into StyleGAN2 latent space

    Args:
        src_dir (str): Directory with aligned images for projection
        dst_dir (str): Output directory
        tmp_dir (str, optional): Temporary directory for tfrecords and video frames. Defaults to ".stylegan2-tmp".
        network_pkl (str, optional): StyleGAN2 network pickle filename. Defaults to 'gdrive:networks/stylegan2-ffhq-config-f.pkl'.
        vgg16_pkl (str, optional): VGG16 network pickle filename. Defaults to 'https://drive.google.com/uc?id=1N2-m9qszOeVC9Tq77WxsLnuWwOedQiD2'.
        num_steps (int, optional): Number of optimization steps. Defaults to 1000.
        initial_learning_rate (float, optional): Initial learning rate. Defaults to 0.1.
        initial_noise_factor (float, optional): Initial noise factor. Defaults to 0.05.
        verbose (bool, optional): Verbose output. Defaults to False.
        video (bool, optional): Render video of the optimization process. Defaults to False.
        video_mode (int, optional): Video mode: 1 for optimization only, 2 for source + optimization. Defaults to 1.
        video_size (int, optional): Video size (height in px). Defaults to 1024.
        video_fps (int, optional): Video framerate. Defaults to 25.
        video_codec (str, optional): Video codec. Defaults to 'libx264'.
        video_bitrate (str, optional): Video bitrate. Defaults to '5M'.
    """
    logger.info('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    proj = projector.Projector(
        vgg16_pkl             = vgg16_pkl,
        num_steps             = num_steps,
        initial_learning_rate = initial_learning_rate,
        initial_noise_factor  = initial_noise_factor,
        verbose               = verbose
    )
    proj.set_network(Gs)

    src_files = sorted([os.path.join(src_dir, f) for f in os.listdir(src_dir) if f[0] not in '._'])
    for src_file in src_files:
        project_image(proj, src_file, dst_dir, tmp_dir, video=video)
        if video:
            render_video(
                src_file, dst_dir, tmp_dir, num_steps, video_mode,
                video_size, video_fps, video_codec, video_bitrate
            )
        shutil.rmtree(tmp_dir)


def main():

    parser = argparse.ArgumentParser(description='Project real-world images into StyleGAN2 latent space')
    parser.add_argument('src_dir', help='Directory with aligned images for projection')
    parser.add_argument('dst_dir', help='Output directory')
    parser.add_argument('--tmp-dir', default='.stylegan2-tmp', help='Temporary directory for tfrecords and video frames')
    parser.add_argument('--network-pkl', default='gdrive:networks/stylegan2-ffhq-config-f.pkl', help='StyleGAN2 network pickle filename')
    parser.add_argument('--vgg16-pkl', default='https://drive.google.com/uc?id=1N2-m9qszOeVC9Tq77WxsLnuWwOedQiD2', help='VGG16 network pickle filename')
    parser.add_argument('--num-steps', type=int, default=1000, help='Number of optimization steps')
    parser.add_argument('--initial-learning-rate', type=float, default=0.1, help='Initial learning rate')
    parser.add_argument('--initial-noise-factor', type=float, default=0.05, help='Initial noise factor')
    parser.add_argument('--verbose', type=bool, default=False, help='Verbose output')
    parser.add_argument('--video', type=bool, default=False, help='Render video of the optimization process')
    parser.add_argument('--video-mode', type=int, default=1, help='Video mode: 1 for optimization only, 2 for source + optimization')
    parser.add_argument('--video-size', type=int, default=1024, help='Video size (height in px)')
    parser.add_argument('--video-fps', type=int, default=25, help='Video framerate')
    parser.add_argument('--video-codec', default='libx264', help='Video codec')
    parser.add_argument('--video-bitrate', default='5M', help='Video bitrate')
    args = parser.parse_args()

    logger.info('Loading networks from "%s"...' % args.network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(args.network_pkl)
    proj = projector.Projector(
        vgg16_pkl             = args.vgg16_pkl,
        num_steps             = args.num_steps,
        initial_learning_rate = args.initial_learning_rate,
        initial_noise_factor  = args.initial_noise_factor,
        verbose               = args.verbose
    )
    proj.set_network(Gs)

    src_files = sorted([os.path.join(args.src_dir, f) for f in os.listdir(args.src_dir) if f[0] not in '._'])
    for src_file in src_files:
        project_image(proj, src_file, args.dst_dir, args.tmp_dir, video=args.video)
        if args.video:
            render_video(
                src_file, args.dst_dir, args.tmp_dir, args.num_steps, args.video_mode,
                args.video_size, args.video_fps, args.video_codec, args.video_bitrate
            )
        shutil.rmtree(args.tmp_dir)


if __name__ == '__main__':
    main()
