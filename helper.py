from gdown import download
from loguru import logger

import string 
import random 


def download_drive_file(file_id, output):
    """
    Download file from google drive

    Args:
        file_id (str): google drive file-id to download
        output (str): path to write file

    Returns:
        Bool: Return True if successfull else False.
    """
    try:
        # download from google drive
        url = f'https://drive.google.com/uc?id={file_id}'
        logger.info("downloading file from google drive.")
        download(url, output, quiet=False)
        return True
    except Exception as e:
        logger.error(e)
        logger.error("Error while downloading file from google drive. Manually add model checkpoint.")
        return False


def generate_unique_str(allow_dashes=True):
    """
    Generate unique string using uuid package

    Args:
        allow_dashes (bool, optional): If true use uuid4() otherwise use hex that will skip dash in names. Defaults to True.
    """
    import uuid

    if allow_dashes:
        unique_str = str(uuid.uuid4())
    else:
        unique_str = uuid.uuid4().hex
    return unique_str


def generate_random_str(length=5):
    """
    generating random strings
    """
    # using random.choices() 
    # generating random strings  
    res = ''.join(
        random.choices(
            string.ascii_uppercase + string.digits,
            k = length
        )
    ) 
    # print("The generated random string : " + str(res)) 
    return res