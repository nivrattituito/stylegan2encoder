import pickle
import PIL.Image
import numpy as np
import dnnlib
import dnnlib.tflib as tflib
import os

def read_feature(file_name):
    file = open(file_name, mode='r')
    # Use readlines() to read the data of all lines and return a list. The data stored in the list is the content of each line
    contents = file.readlines()
    # Prepare a list to store the retrieved data
    code = np.zeros((512, ))
    # for loops through the list, removing the content read in each line
    for i in range(512):
        name = contents[i]
        name = name.strip('\n')
        code[i] = name
    code = np.float32(code)
    file.close()
    return code

def move_latent_and_save(latent_vector, direction_file, coeffs, Gs_network, Gs_syn_kwargs):
    direction = np.load('latent_directions/' + direction_file)
    os.makedirs('results/'+direction_file.split('.')[0], exist_ok=True)
    '''latent_vector is the latent encoding of the face, direction is the direction of face adjustment, coeffs is the vector of changing stride, and generator is the generator'''
    for i, coeff in enumerate(coeffs):
        new_latent_vector = latent_vector.copy()
        new_latent_vector[0][:8] = (latent_vector[0] + coeff*direction)[:8]
        images = Gs_network.components.synthesis.run(new_latent_vector, **Gs_syn_kwargs)
        result = PIL.Image.fromarray(images[0],'RGB')
        result.save('results/'+direction_file.split('.')[0]+'/'+str(i).zfill(3)+'.png')


def main():

    # Choose generator here
    tflib.init_tf()
    with open('networks/generator_yellow-stylegan2-config-f.pkl', "rb") as f:
        generator_network, discriminator_network, Gs_network = pickle.load(f)

    # These are some configuration parameters, don't touch it
    w_avg = Gs_network.get_var('dlatent_avg')
    noise_vars = [var for name, var in Gs_network.components.synthesis.vars.items() if name.startswith('noise')]
    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = 1
    truncation_psi = 0.5

    # Choose the latent code of the character here, and pay attention to matching it with the generator. The latent code comes from a txt file in the generate_codes folder in the generate directory.
    face_latent = read_feature('results/generate_codes/0000.txt')
    z = np.stack(face_latent for _ in range(1))
    tflib.set_vars({var: np.random.randn(*var.shape.as_list()) for var in noise_vars}) # [height, width]
    w = Gs_network.components.mapping.run(z, None)
    w = w_avg + (w-w_avg) * truncation_psi

    # Choose the adjustment direction here. There are 21 adjustment methods in total. Their names and their corresponding functions are as follows.
    '''
        age.npy-adjust age
        angle_horizontal.npy-adjust the face angle in the left and right directions
        angle_vertical.npy-adjust the face angle in the up and down direction
        beauty.npy-adjust beauty
        emotion_angry.npy-adjust this item to increase/decrease some angry emotions (adjusting the step size is recommended to reduce)
        emotion_disgust.npy-adjust this item to add/reduce some disgusting emotions (adjusting the step size is recommended to reduce)
        emotion_easy.npy-adjust this item to add/weak some calm emotions (adjusting the step size is recommended to reduce)
        emotion_fear.npy-adjust this item to increase/decrease some emotions of fear (adjusting the step size is recommended to reduce)
        emotion_happy.npy-adjust this item to add/reduce some happy emotions (adjusting the step size is recommended to reduce)
        emotion_sad.npy-adjust this item to add/reduce some sad emotions (adjusting the step size is recommended to reduce)
        emotion_surprise.npy-adjust this item to add/reduce some emotions of surprise (adjusting the step size is recommended to shrink)
        eyes_open.npy-adjust the degree of eye closure
        face_shape.npy-adjust face shape
        gender.npy-adjust gender
        glasses.npy-adjust whether to wear glasses
        height.npy-adjust the height of the face
        race_black.npy-adjust this option to approach/away to black people
        race_white.npy-adjust this item to approach/away to white race
        race_yellow.npy-adjust this option to approach/away from the yellow race
        smile.npy-adjust smile
        width.npy-adjust the width of the face
    '''
    direction_file ='smile.npy' # Choose one of the edit vectors above

    # Choose the size of adjustment here. The value in the vector represents the adjustment range and you can edit it yourself. For each value, a picture will be generated and saved.
    coeffs = [-15., -12., -9., -6., -3., 0., 3., 6., 9., 12.]

    # Start to adjust and save the picture
    move_latent_and_save(w, direction_file, coeffs, Gs_network, Gs_syn_kwargs)


if __name__ == "__main__":
    main()