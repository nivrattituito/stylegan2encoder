"""
Actions to Generate de-aged faces of diffrent angles , emotions 
"""
# tasks = [
#     {
#     "number_of_frames": 10,
#     "manipulation": [{
#             "parameter"      : 'age',
#             "intensity"      : -8.8,
#             "boost_intensity": False,
#             "locked_feature" : True,
#         },
#         {
#             "parameter"      : 'angle_horizontal',
#             "intensity"      : 19.8,
#             "boost_intensity": False,
#             "locked_feature" : False,
#         },
#         {
#             "parameter"      : 'angle_vertical',
#             "intensity"      : 20,
#             "boost_intensity": False,
#             "locked_feature" : False,
#         }],
#     },
# ]

tasks = [
    {
        "name"            : 'move face towards top-left corner',
        "number_of_frames": 20,
        "manipulation"    : [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : 19.8,
            },
            {
                "parameter"      : 'angle_vertical',
                "intensity"      : 20,
                "boost_intensity": False,
                "locked_feature" : False,
            },
        ],
    },
    {
        "name"            : 'move face towards top-right corner',
        "number_of_frames": 20,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : -19.8,
            },
            {
                "parameter"      : 'angle_vertical',
                "intensity"      : 20,
            }
        ],
    },
    {
        "name"            : 'move face towards bottom-right corner',
        "number_of_frames": 20,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : 19.8,
            },
            {
                "parameter"      : 'angle_vertical',
                "intensity"      : -20,
            }
        ],
    },
    {
        "name"            : 'move face towards bottom-left corner',
        "number_of_frames": 20,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : -19.8, # minus 
            },
            {
                "parameter"      : 'angle_vertical',
                "intensity"      : -20,   # vertical also minus
            }
        ],
    },
    {
        "name"            : 'move face towards right side with happy emotion',
        "number_of_frames": 40,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : 40.2,
            },
            {
                "parameter"      : 'emotion_happy',
                "intensity"      : 5,
            }
        ],
    },
    {
        "name"            : 'move face towards left side with happy emotion',
        "number_of_frames": 40,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : -40.2,
            },
            {
                "parameter"      : 'emotion_happy',
                "intensity"      : 5,
            }
        ],
    },
    {
        "name"            : 'move face towards left side with anger emotion',
        "number_of_frames": 10,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : 40.2,
            },
            {
                "parameter"      : 'emotion_angry',
                "intensity"      : 5,
            }
        ],
    },
    {
        "name"            : 'move face towards left side with anger emotion',
        "number_of_frames": 10,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : -40.2,
            },
            {
                "parameter"      : 'emotion_angry',
                "intensity"      : 5,
            }
        ],
    },
    {
        "name"            : 'Smile',
        "number_of_frames": 10,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'smile',
                "intensity"      : +10,
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : -1.2,
            },
        ],
    },
    {
        "name"            : 'Open close eyes',
        "number_of_frames": 20,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : -1.2,
            },
            {
                "parameter"      : 'eyes_open',
                "intensity"      : +20,
            },
        ],
    },
    {
        "name"            : 'mouth closing',
        "number_of_frames": 10,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : +1.2,
            },
            {
                "parameter"      : 'mouth_open',
                "intensity"      : -90,
            },
        ],
    },
    {
        "name"            : 'mouth opening',
        "number_of_frames": 8,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : +1.2,
            },
            {
                "parameter"      : 'mouth_open',
                "intensity"      : 80,
            },
        ],
    },
]

# ---------------------------------------------------------------
demo_tasks = [
    {
        "name"            : 'mouth opening',
        "number_of_frames": 8,
        "manipulation": [{
                "parameter"      : 'age',
                "_comment"       : 'use global values of intensity',
            },
            {
                "parameter"      : 'angle_horizontal',
                "intensity"      : +1.2,
            },
            {
                "parameter"      : 'mouth_open',
                "intensity"      : 80,
            },
        ],
    },
]