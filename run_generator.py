# Copyright (c) 2019, NVIDIA Corporation. All rights reserved.
#
# This work is made available under the Nvidia Source Code License-NC.
# To view a copy of this license, visit
# https://nvlabs.github.io/stylegan2/license.html

import argparse
import json
import numpy as np
import dnnlib
import dnnlib.tflib as tflib
import re
import os, sys
import pretrained_networks
from tqdm import tqdm

from pathlib import Path
import io
import base64
from glob import glob

try:
    import PIL.Image
except:
    try:
        import Image
    except:
        import PIL


def generate_images(network_pkl, seeds, num, truncation_psi):
    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    noise_vars = [var for name, var in Gs.components.synthesis.vars.items() if name.startswith('noise')]

    Gs_kwargs = dnnlib.EasyDict()
    Gs_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
    Gs_kwargs.randomize_noise = False
    if truncation_psi is not None:
        Gs_kwargs.truncation_psi = truncation_psi

    if seeds is None:
        actual_seeds = np.random.RandomState().randint(0, 2 ** 32, num)
    else:
        actual_seeds = seeds
    for seed_idx, seed in enumerate(actual_seeds):
        print('Generating image for seed %d (%d/%d) ...' % (seed, seed_idx, len(actual_seeds)))
        rnd = np.random.RandomState(seed)
        z = rnd.randn(1, *Gs.input_shape[1:]) # [minibatch, component]
        tflib.set_vars({var: rnd.randn(*var.shape.as_list()) for var in noise_vars}) # [height, width]
        images = Gs.run(z, None, **Gs_kwargs) # [minibatch, height, width, channel]
        PIL.Image.fromarray(images[0], 'RGB').save(dnnlib.make_run_dir_path('img%05d.png' % seed_idx if seeds is None else 'seed%04d.png' % seed))

def generate_images_latent(
    network_pkl, num, truncation_psi,
    minibatch_size, output_dir, out_imgsize=1024
    ):
    """
    Generate images with latent vector

    Args:
        network_pkl ([type]): [description]
        num ([type]): [description]
        truncation_psi ([type]): [description]
        minibatch_size ([type]): [description]
        output_dir ([type]): [description]
        out_imgsize (int, optional): [description]. Defaults to (1024).
    """
    result_dir = Path(dnnlib.submit_config.run_dir_root)
    output_dir = Path(output_dir)

    images_dir = result_dir / 'images'
    dlatents_dir = result_dir / 'dlatents'

    images_dir.mkdir(exist_ok=True, parents=True)
    dlatents_dir.mkdir(exist_ok=True, parents=True)

    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    w_avg = Gs.get_var('dlatent_avg')

    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8,
                                          nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = minibatch_size

    latents_from = 0
    latents_to = 8

    for i in tqdm(range(num // minibatch_size)):
        all_z = np.random.randn(minibatch_size, *Gs.input_shape[1:])
        all_w = Gs.components.mapping.run(all_z, None)
        all_w = w_avg + (all_w - w_avg) * truncation_psi

        all_images = Gs.components.synthesis.run(all_w, **Gs_syn_kwargs)

        for j, (dlatent, image) in enumerate(zip(all_w, all_images)):
            image_pil = PIL.Image.fromarray(image, 'RGB')

            if out_imgsize and out_imgsize != 1024:
                ## Default out_imgsize 1024 which is eual to stylegan2 output
                # resize image
                image_pil = image_pil.resize((out_imgsize, out_imgsize))

            image_pil.save(images_dir / (str(i * minibatch_size + j) + '.png'))
            np.save(
                dlatents_dir / (str(i * minibatch_size + j) + '_img' + '.npy'),
                dlatent[0]
            )


def generate_images_custom_age(
    network_pkl, num, truncation_psi,
    minibatch_size, output_dir,
    direction_path, coeff
    ):
    """
    Generate custom images
    -- Generate image first -- from that image generate younger and older face image 
    -- also save their latent vector

    Args:
        network_pkl ([type]): [description]
        num ([type]): [description]
        truncation_psi ([type]): [description]
        minibatch_size ([type]): [description]
        output_dir ([type]): [description]
        direction_path ([type]): [description]
        coeff ([type]): [description]
    """
    result_dir = Path(dnnlib.submit_config.run_dir_root)
    output_dir = Path(output_dir)

    if direction_path is not None:
        direction = np.load(direction_path)

    images_dir = result_dir / 'images'
    dlatents_dir = result_dir / 'dlatents'
    output_tsv = output_dir / 'out.tsv'

    images_dir.mkdir(exist_ok=True, parents=True)
    dlatents_dir.mkdir(exist_ok=True, parents=True)
    output_dir.mkdir(exist_ok=True)

    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    w_avg = Gs.get_var('dlatent_avg')

    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8,
                                          nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = minibatch_size

    latents_from = 0
    latents_to = 8

    for i in tqdm(range(num // minibatch_size)):
        all_z = np.random.randn(minibatch_size, *Gs.input_shape[1:])
        all_w = Gs.components.mapping.run(all_z, None)
        all_w = w_avg + (all_w - w_avg) * truncation_psi

        if direction_path is not None:
            assert coeff is not None
            pos_w = all_w.copy()
            neg_w = all_w.copy()

            for j in range(len(all_w)):
                pos_w[j][latents_from:latents_to] = \
                    (pos_w[j] + coeff * direction)[latents_from:latents_to]
                neg_w[j][latents_from:latents_to] = \
                    (neg_w[j] - coeff * direction)[latents_from:latents_to]

            pos_images = Gs.components.synthesis.run(pos_w,
                                                     **Gs_syn_kwargs)
            neg_images = Gs.components.synthesis.run(neg_w,
                                                     **Gs_syn_kwargs)

            for j in range(len(all_w)):
                pos_image_pil = PIL.Image.fromarray(pos_images[j], 'RGB')
                pos_image_pil.save(
                    images_dir / '{}_tr_{}.png'.format(i * minibatch_size +
                                                       j, coeff))

                neg_image_pil = PIL.Image.fromarray(neg_images[j], 'RGB')
                neg_image_pil.save(
                    images_dir / '{}_tr_-{}.png'.format(i * minibatch_size +
                                                       j, coeff))

        all_images = Gs.components.synthesis.run(all_w, **Gs_syn_kwargs)

        for j, (dlatent, image) in enumerate(zip(all_w, all_images)):
            image_pil = PIL.Image.fromarray(image, 'RGB')
            image_pil.save(images_dir / (str(i * minibatch_size + j) + '_img' + '.png'))
            np.save(dlatents_dir / (str(i * minibatch_size + j) + '_img' + '.npy'),
                    dlatent[0])


def generate_images_custom_younger(network_pkl, num, truncation_psi,
                           minibatch_size, output_dir,
                           direction_path, coeff, out_imgsize=256, is_save_dlatent=True):
    result_dir = Path(dnnlib.submit_config.run_dir_root)
    output_dir = Path(output_dir)

    # use tw folder directions
    if direction_path is not None:
        direction = np.load(direction_path, allow_pickle=True)
    else:
        direction = np.load("latent_directions/tw/age.npy", allow_pickle=True)

    images_source_dir = result_dir / 'images/source'
    images_younger_dir = result_dir / 'images/younger'
    dlatents_dir = result_dir / 'dlatents'
    # output_tsv = output_dir / 'out.tsv'

    images_source_dir.mkdir(exist_ok=True, parents=True)
    images_younger_dir.mkdir(exist_ok=True, parents=True)

    if is_save_dlatent:
      dlatents_dir.mkdir(exist_ok=True, parents=True)
    # output_dir.mkdir(exist_ok=True)

    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    w_avg = Gs.get_var('dlatent_avg')

    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8,
                                          nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = minibatch_size

    latents_from = 0
    latents_to = 8

    for i in tqdm(range(num // minibatch_size)):
        all_z = np.random.randn(minibatch_size, *Gs.input_shape[1:])
        all_w = Gs.components.mapping.run(all_z, None)
        all_w = w_avg + (all_w - w_avg) * truncation_psi

        if direction_path is not None:
            assert coeff is not None
            # pos_w = all_w.copy()
            neg_w = all_w.copy()

            for j in range(len(all_w)):
                # pos_w[j][latents_from:latents_to] = \
                #     (pos_w[j] + coeff * direction)[latents_from:latents_to]
                neg_w[j][latents_from:latents_to] = \
                    (neg_w[j] - coeff * direction)[latents_from:latents_to]

            # pos_images = Gs.components.synthesis.run(pos_w,
            #                                          **Gs_syn_kwargs)
            neg_images = Gs.components.synthesis.run(neg_w,
                                                     **Gs_syn_kwargs)

            for j in range(len(all_w)):
                # pos_image_pil = PIL.Image.fromarray(pos_images[j], 'RGB')
                # pos_image_pil.save(
                #     images_dir / '{}_tr_{}.png'.format(i * minibatch_size +
                #                                        j, coeff))

                neg_image_pil = PIL.Image.fromarray(neg_images[j], 'RGB')
                if out_imgsize and out_imgsize != 1024:
                    ## Default out_imgsize 1024 which is equal to stylegan2 output
                    # resize image
                    neg_image_pil = neg_image_pil.resize((out_imgsize, out_imgsize))

                # neg_image_pil.save(
                #     images_dir / '{}_tr_-{}.png'.format(i * minibatch_size +
                #                                        j, coeff))
                neg_image_pil.save(images_younger_dir / (str(i * minibatch_size + j) + '.png'))

        all_images = Gs.components.synthesis.run(all_w, **Gs_syn_kwargs)

        for j, (dlatent, image) in enumerate(zip(all_w, all_images)):
            image_pil = PIL.Image.fromarray(image, 'RGB')
            if out_imgsize and out_imgsize != 1024:
                ## Default out_imgsize 1024 which is equal to stylegan2 output
                # resize image
                image_pil = image_pil.resize((out_imgsize, out_imgsize))

            image_pil.save(images_source_dir / (str(i * minibatch_size + j) + '.png'))

            if is_save_dlatent:
                # save dlatent - for modification later
                np.save(
                    dlatents_dir / (str(i * minibatch_size + j) + '_img' + '.npy'),
                    dlatent[0]
                )


def generate_images_custom_younger_v2(
    network_pkl, num, truncation_psi,
    minibatch_size, output_dir,
    direction_path, coeff, out_imgsize=256, is_save_dlatent=True, 
    younger_max_age_threshold=18
    ):
    from facelib import FaceDetector, AgeGenderEstimator
    face_detector = FaceDetector()
    age_gender_detector = AgeGenderEstimator()

    result_dir = Path(dnnlib.submit_config.run_dir_root)
    output_dir = Path(output_dir)

    # use tw folder directions
    if direction_path is not None:
        direction = np.load(direction_path, allow_pickle=True)
    else:
        direction = np.load("latent_directions/tw/age.npy", allow_pickle=True)

    images_source_dir = Path(os.path.join(result_dir, 'images', 'source'))
    images_younger_dir = Path(os.path.join(result_dir, 'images', 'younger'))
    dlatents_dir = Path(os.path.join(result_dir, 'dlatents'))
    # output_tsv = output_dir / 'out.tsv'

    images_source_dir.mkdir(exist_ok=True, parents=True)
    images_younger_dir.mkdir(exist_ok=True, parents=True)

    if is_save_dlatent:
        dlatents_dir.mkdir(exist_ok=True, parents=True)
    # output_dir.mkdir(exist_ok=True)

    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    w_avg = Gs.get_var('dlatent_avg')

    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8,
                                          nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = minibatch_size

    latents_from = 0
    latents_to = 8

    for i in tqdm(range(num // minibatch_size)):
        all_z = np.random.randn(minibatch_size, *Gs.input_shape[1:])
        all_w = Gs.components.mapping.run(all_z, None)
        all_w = w_avg + (all_w - w_avg) * truncation_psi

        ## If younger face below threshold add it here
        valid_youngers_idx = [] # valid younger indexes

        if direction_path is not None:
            assert coeff is not None
            # pos_w = all_w.copy()
            neg_w = all_w.copy()

            for j in range(len(all_w)):
                # pos_w[j][latents_from:latents_to] = \
                #     (pos_w[j] + coeff * direction)[latents_from:latents_to]
                neg_w[j][latents_from:latents_to] = \
                    (neg_w[j] - coeff * direction)[latents_from:latents_to]

            # pos_images = Gs.components.synthesis.run(pos_w,
            #                                          **Gs_syn_kwargs)
            neg_images = Gs.components.synthesis.run(
                neg_w,
                **Gs_syn_kwargs
            )

            for j in range(len(all_w)):
                ## Check is generated younger face below threshold
                faces, boxes, scores, landmarks = face_detector.detect_align(neg_images[j])
                genders, ages = age_gender_detector.detect(faces)
                # print(genders, ages)
                if ages:
                    age = ages[0]
                else:
                    age = None

                if age and age < younger_max_age_threshold:
                    valid_youngers_idx.append(j)
                else:
                    continue # skip saving

                # pos_image_pil = PIL.Image.fromarray(pos_images[j], 'RGB')
                # pos_image_pil.save(
                #     images_dir / '{}_tr_{}.png'.format(i * minibatch_size +
                #                                        j, coeff))
                neg_image_pil = PIL.Image.fromarray(neg_images[j], 'RGB')
                if out_imgsize and out_imgsize != 1024:
                    ## Default out_imgsize 1024 which is equal to stylegan2 output
                    # resize image
                    neg_image_pil = neg_image_pil.resize((out_imgsize, out_imgsize))

                neg_image_pil.save(images_younger_dir / (str(i * minibatch_size + j) + '.png'))
                
        all_images = Gs.components.synthesis.run(all_w, **Gs_syn_kwargs)

        for j, (dlatent, image) in enumerate(zip(all_w, all_images)):
            ## Check if not in valid
            if j not in valid_youngers_idx:
                continue

            image_pil = PIL.Image.fromarray(image, 'RGB')
            if out_imgsize and out_imgsize != 1024:
                ## Default out_imgsize 1024 which is equal to stylegan2 output
                # resize image
                image_pil = image_pil.resize((out_imgsize, out_imgsize))

            image_pil.save(images_source_dir / (str(i * minibatch_size + j) + '.png'))

            if is_save_dlatent:
                # save dlatent - for modification later
                np.save(
                    dlatents_dir / (str(i * minibatch_size + j) + '.npy'),
                    dlatent[0]
                )


def apply_vector_on_backprop(network_pkl, dlatents_files_pattern,
                             direction_path, transformation):
    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    dlatent_steps = [5, 10, 50, 100, 200, 400, 600, 800, 1000]

    Gs_kwargs = dnnlib.EasyDict()
    Gs_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8,
                                      nchw_to_nhwc=True)
    Gs_kwargs.randomize_noise = False

    if transformation == 'gender':
        with open(direction_path) as f:
            delta = np.array([float(i) for i in f.readlines()[0].split()])
            if len(delta) == 512:
                delta = np.tile(delta, [18, 1])
            else:
                raise Exception('Wrong direction vector')
    elif transformation == 'age':
        with open(direction_path) as f:
            age = json.loads(f.read().replace("'", '"'))
        delta = np.array(age['18-24']) - np.array(age['45-54'])  # 55-130
        if len(delta) == 512:
            delta = np.tile(delta, [18, 1])
        else:
            raise Exception('Wrong direction vector')
    else:
        raise ValueError('Wrong transformation value')

    dlatents_files = glob(dlatents_files_pattern + '*step.txt')
    for file in tqdm(dlatents_files):
        person_name = file.split('/')[-1].split('image')[-1].split('-')[0]
        with open(file) as f:
            for step, line in zip(dlatent_steps, f.readlines()):
                if step in [200, 1000]:  # [10, 50, 100, 200, 1000]:
                    dlatent = np.array([float(i) for i in line.split()])
                    if len(dlatent) == 512:
                        dlatent = np.tile(dlatent, [18, 1])
                    elif len(dlatent) == 512 * 18:
                        dlatent = np.reshape(dlatent, [18, 512])
                    else:
                        raise Exception('Wrong person vector')

                    dlatents_person = []
                    fnames = []

                    alphas = [-1.0 + 0.25 * i for i in range(9) if i not in [
                        3, 5]]
                    for alpha in alphas:
                        dlatents_person.append(dlatent + alpha * delta)
                        fnames.append('translation_%s_%s_%s.png' % (person_name,
                                                                    step,
                                                                    alpha))

                    images = Gs.components.synthesis.run(np.array(dlatents_person),
                                                         **Gs_kwargs)

                    for fname, image in zip(fnames, images):
                        PIL.Image.fromarray(image, 'RGB').save(
                            dnnlib.make_run_dir_path(fname))


def style_mixing_example(network_pkl, row_seeds, col_seeds, truncation_psi, col_styles, minibatch_size=4):
    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    w_avg = Gs.get_var('dlatent_avg') # [component]

    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = minibatch_size

    print('Generating W vectors...')
    all_seeds = list(set(row_seeds + col_seeds))
    all_z = np.stack([np.random.RandomState(seed).randn(*Gs.input_shape[1:]) for seed in all_seeds]) # [minibatch, component]
    all_w = Gs.components.mapping.run(all_z, None) # [minibatch, layer, component]
    all_w = w_avg + (all_w - w_avg) * truncation_psi # [minibatch, layer, component]
    w_dict = {seed: w for seed, w in zip(all_seeds, list(all_w))} # [layer, component]

    print('Generating images...')
    all_images = Gs.components.synthesis.run(all_w, **Gs_syn_kwargs) # [minibatch, height, width, channel]
    image_dict = {(seed, seed): image for seed, image in zip(all_seeds, list(all_images))}

    print('Generating style-mixed images...')
    for row_seed in row_seeds:
        for col_seed in col_seeds:
            w = w_dict[row_seed].copy()
            w[col_styles] = w_dict[col_seed][col_styles]
            image = Gs.components.synthesis.run(w[np.newaxis], **Gs_syn_kwargs)[0]
            image_dict[(row_seed, col_seed)] = image

    print('Saving images...')
    for (row_seed, col_seed), image in image_dict.items():
        PIL.Image.fromarray(image, 'RGB').save(dnnlib.make_run_dir_path('%d-%d.png' % (row_seed, col_seed)))

    print('Saving image grid...')
    _N, _C, H, W = Gs.output_shape
    canvas = PIL.Image.new('RGB', (W * (len(col_seeds) + 1), H * (len(row_seeds) + 1)), 'black')
    for row_idx, row_seed in enumerate([None] + row_seeds):
        for col_idx, col_seed in enumerate([None] + col_seeds):
            if row_seed is None and col_seed is None:
                continue
            key = (row_seed, col_seed)
            if row_seed is None:
                key = (col_seed, col_seed)
            if col_seed is None:
                key = (row_seed, row_seed)
            canvas.paste(PIL.Image.fromarray(image_dict[key], 'RGB'), (W * col_idx, H * row_idx))
    canvas.save(dnnlib.make_run_dir_path('grid.png'))

#----------------------------------------------------------------------------

def _parse_num_range(s):
    '''Accept either a comma separated list of numbers 'a,b,c' or a range 'a-c' and return as a list of ints.'''

    range_re = re.compile(r'^(\d+)-(\d+)$')
    m = range_re.match(s)
    if m:
        return range(int(m.group(1)), int(m.group(2))+1)
    vals = s.split(',')
    return [int(x) for x in vals]

#----------------------------------------------------------------------------

_examples = '''examples:

  # Generate ffhq uncurated images (matches paper Figure 12)
  python %(prog)s generate-images --network=gdrive:networks/stylegan2-ffhq-config-f.pkl --seeds=6600-6625 --truncation-psi=0.5

  # Generate ffhq curated images (matches paper Figure 11)
  python %(prog)s generate-images --network=gdrive:networks/stylegan2-ffhq-config-f.pkl --seeds=66,230,389,1518 --truncation-psi=1.0

  # Generate uncurated car images (matches paper Figure 12)
  python %(prog)s generate-images --network=gdrive:networks/stylegan2-car-config-f.pkl --seeds=6000-6025 --truncation-psi=0.5

  # Generate style mixing example (matches style mixing video clip)
  python %(prog)s style-mixing-example --network=gdrive:networks/stylegan2-ffhq-config-f.pkl --row-seeds=85,100,75,458,1500 --col-seeds=55,821,1789,293 --truncation-psi=1.0
'''

#----------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(
        description='''StyleGAN2 generator.

        Run 'python %(prog)s <subcommand> --help' for subcommand help.''',
        epilog=_examples,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    subparsers = parser.add_subparsers(help='Sub-commands', dest='command')

    parser_generate_images = subparsers.add_parser('generate-images', help='Generate images')
    parser_generate_images.add_argument('--network', help='Network pickle filename', dest='network_pkl', required=True)
    parser_generate_images.add_argument('--seeds', type=_parse_num_range, help='List of random seeds', default=None)
    parser_generate_images.add_argument('--num', type=int, help='Num images to generate', default=16)
    parser_generate_images.add_argument('--truncation-psi', type=float, help='Truncation psi (default: %(default)s)', default=0.5)
    parser_generate_images.add_argument('--result-dir', help='Root directory for run results (default: %(default)s)', default='results', metavar='DIR')

    ## Custom
    ## -----------------------------------------------------------------------------------
    ## generate_images_latent
    ## -----------------------------------------------------------------------------------
    generate_images_latent = subparsers.add_parser(
        'generate-images-latent', 
        help='Generate images with dlatents'
    )
    generate_images_latent.add_argument(
        '--network',
        help='Network pickle filename',
        dest='network_pkl', required=True
    )
    generate_images_latent.add_argument(
        '--num', type=int,
        help='Num images to generate',
        default=16
    )
    generate_images_latent.add_argument(
        '--truncation-psi', type=float,
        help='Truncation psi (default: %(default)s)',
        default=0.5
    )
    generate_images_latent.add_argument(
        '--result-dir',
        help='Root directory for run results (default: %(default)s)',
        default='results', metavar='DIR'
    )
    generate_images_latent.add_argument(
        '--minibatch_size', type=int,
        help='Minibatch size',
        default=8
    )
    generate_images_latent.add_argument(
        '--output_dir',
        help='Root directory for output tsv',
        default='outputs'
    )

    ## -----------------------------------------------------------------------------------
    ## -----------------------------------------------------------------------------------
    ## generate_images_custom_age
    ## -----------------------------------------------------------------------------------
    parser_generate_images_custom_age = subparsers.add_parser(
        'generate-images-custom', 
        help='Generate images with dlatents -- image, child, and older face'
    )
    parser_generate_images_custom_age.add_argument(
        '--network',
        help='Network pickle filename',
        dest='network_pkl', required=True
    )
    parser_generate_images_custom_age.add_argument(
        '--num', type=int,
        help='Num images to generate',
        default=16
    )
    parser_generate_images_custom_age.add_argument(
        '--truncation-psi', type=float,
        help='Truncation psi (default: %(default)s)',
        default=0.5
    )
    parser_generate_images_custom_age.add_argument(
        '--result-dir',
        help='Root directory for run results (default: %(default)s)',
        default='results', metavar='DIR'
    )
    parser_generate_images_custom_age.add_argument(
        '--minibatch_size', type=int,
        help='Minibatch size',
        default=8
    )
    parser_generate_images_custom_age.add_argument(
        '--output_dir',
        help='Root directory for output tsv',
        default='outputs'
    )
    parser_generate_images_custom_age.add_argument('--direction_path', default=None)
    parser_generate_images_custom_age.add_argument(
        '--coeff', dest='coeff',
        type=float
    )

    # ---------------------------------------------------------------------------------
    ## -----------------------------------------------------------------------------------
    ## generate_images_custom_younger
    ## -----------------------------------------------------------------------------------
    generate_images_custom_younger = subparsers.add_parser(
        'generate-images-custom-younger', help='Generate images with younger face pair'
    )
    generate_images_custom_younger.add_argument(
        '--network',
        help='Network pickle filename',
        dest='network_pkl', required=True
    )
    generate_images_custom_younger.add_argument(
        '--num', type=int,
        help='Num images to generate',
        default=16
    )
    generate_images_custom_younger.add_argument(
        '--truncation-psi', type=float,
        help='Truncation psi (default: %(default)s)',
        default=0.5
    )
    generate_images_custom_younger.add_argument(
        '--result-dir',
        help='Root directory for run results (default: %(default)s)',
        default='results', metavar='DIR'
    )
    generate_images_custom_younger.add_argument(
        '--minibatch_size', type=int,
        help='Minibatch size',
        default=8
    )
    generate_images_custom_younger.add_argument(
        '--output_dir',
        help='Root directory for output tsv',
        default='outputs'
    )
    generate_images_custom_younger.add_argument(
      '--direction_path', 
      default="latent_directions/tw/age.npy"
    )
    generate_images_custom_younger.add_argument(
        '--coeff', dest='coeff',
        help='Value to control age change',
        type=float
    )
    generate_images_custom_younger.add_argument(
        '--out_imgsize', type=int,
        help='Output Image size',
        default=256
    )
    generate_images_custom_younger.add_argument(
        '--is_save_dlatent', type=bool,
        help='Flag to control saving of dlantent',
        default=True
    )

    # ---------------------------------------------------------------------------------
    ## -----------------------------------------------------------------------------------
    ## generate_images_custom_younger_v2
    ## -----------------------------------------------------------------------------------
    generate_images_custom_younger_v2 = subparsers.add_parser(
        'generate-images-custom-younger-v2', help='Generate images with younger face pair with age filtering'
    )
    generate_images_custom_younger_v2.add_argument(
        '--network',
        help='Network pickle filename',
        dest='network_pkl', required=True
    )
    generate_images_custom_younger_v2.add_argument(
        '--num', type=int,
        help='Num images to generate',
        default=16
    )
    generate_images_custom_younger_v2.add_argument(
        '--truncation-psi', type=float,
        help='Truncation psi (default: %(default)s)',
        default=0.5
    )
    generate_images_custom_younger_v2.add_argument(
        '--result-dir',
        help='Root directory for run results (default: %(default)s)',
        default='results', metavar='DIR'
    )
    generate_images_custom_younger_v2.add_argument(
        '--minibatch_size', type=int,
        help='Minibatch size',
        default=8
    )
    generate_images_custom_younger_v2.add_argument(
        '--output_dir',
        help='Root directory for output tsv',
        default='outputs'
    )
    generate_images_custom_younger_v2.add_argument(
      '--direction_path', 
      default="latent_directions/tw/age.npy"
    )
    generate_images_custom_younger_v2.add_argument(
        '--coeff', dest='coeff',
        help='Value to control age change',
        type=float
    )
    generate_images_custom_younger_v2.add_argument(
        '--out_imgsize', type=int,
        help='Output Image size',
        default=256
    )
    generate_images_custom_younger_v2.add_argument(
        '--is_save_dlatent', type=bool,
        help='Flag to control saving of dlantent',
        default=True
    )
    generate_images_custom_younger_v2.add_argument(
        '--younger_max_age_threshold', type=int,
        help='Max age for younger',
        default=18
    )

    # --------------------------------------------------------------------------------
    ## -----------------------------------------------------------------------------------
    ## apply_vector_on_backprop
    ## -----------------------------------------------------------------------------------
    parser_apply_vector_on_backprop = subparsers.add_parser(
        'apply-vector-on-backprop', 
        help='Transform image with direction'
    )
    parser_apply_vector_on_backprop.add_argument(
        '--network',
        help='Network pickle filename',
        dest='network_pkl',
        required=True
    )
    parser_apply_vector_on_backprop.add_argument(
        '--dlatents_files_pattern',
        default=None
    )
    parser_apply_vector_on_backprop.add_argument('--direction_path', default=None)
    parser_apply_vector_on_backprop.add_argument(
        '--transformation',
        default=None
    )
    parser_apply_vector_on_backprop.add_argument(
        '--result-dir',
        help='Root directory for run results (default: %(default)s)', default='results', metavar='DIR'
    )

    parser_style_mixing_example = subparsers.add_parser('style-mixing-example', help='Generate style mixing video')
    parser_style_mixing_example.add_argument('--network', help='Network pickle filename', dest='network_pkl', required=True)
    parser_style_mixing_example.add_argument('--row-seeds', type=_parse_num_range, help='Random seeds to use for image rows', required=True)
    parser_style_mixing_example.add_argument('--col-seeds', type=_parse_num_range, help='Random seeds to use for image columns', required=True)
    parser_style_mixing_example.add_argument('--col-styles', type=_parse_num_range, help='Style layer range (default: %(default)s)', default='0-6')
    parser_style_mixing_example.add_argument('--truncation-psi', type=float, help='Truncation psi (default: %(default)s)', default=0.5)
    parser_style_mixing_example.add_argument('--result-dir', help='Root directory for run results (default: %(default)s)', default='results', metavar='DIR')

    args = parser.parse_args()
    kwargs = vars(args)
    subcmd = kwargs.pop('command')

    if subcmd is None:
        print('Error: missing subcommand.  Re-run with --help for usage.')
        sys.exit(1)

    sc = dnnlib.SubmitConfig()
    sc.num_gpus = 1
    sc.submit_target = dnnlib.SubmitTarget.LOCAL
    sc.local.do_not_copy_source_files = True
    sc.run_dir_root = kwargs.pop('result_dir')
    sc.run_desc = subcmd

    func_name_map = {
        'generate-images': 'run_generator.generate_images',
        'generate-images-latent': 'run_generator.generate_images_latent',
        'generate-images-custom-age': 'run_generator.generate_images_custom_age',
        'generate-images-custom-younger': 'run_generator.generate_images_custom_younger',
        'generate-images-custom-younger-v2': 'run_generator.generate_images_custom_younger_v2',
        'apply-vector-on-backprop': 'run_generator.apply_vector_on_backprop',
        'style-mixing-example': 'run_generator.style_mixing_example'
    }
    dnnlib.submit_run(sc, func_name_map[subcmd], **kwargs)


if __name__ == "__main__":
    main()
