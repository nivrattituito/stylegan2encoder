import os
import dlib
import numpy as np
from loguru import logger


class LandmarksDetector:
    def __init__(self, predictor_model_path):
        """
        Get 68 face landmarks using dlib

        Args:
            predictor_model_path (str): path to shape_predictor_68_face_landmarks.dat file
        """
        self.detector = dlib.get_frontal_face_detector()
        self.shape_predictor = dlib.shape_predictor(predictor_model_path)

    def get_landmarks(self, image):
        """
        Get 68 face landmarks computed by dlib

        Args:
            image (str | ndarray): Source image to compute face landmarks

        Yields:
            list: face landmarks
        """
        if isinstance(image, str):
            if not os.path.isfile(image):
                logger.error(f'\nCannot find source image {image}.')
                return []
            image = dlib.load_rgb_image(image)

        elif isinstance(image, np.ndarray):
            pass
        else:
            logger.error(f'\nUnknown input image type {type(image)}. Valid input type: string, ndarray')
            return []
            
        dets = self.detector(image, 1)

        for detection in dets:
            face_landmarks = [(item.x, item.y) for item in self.shape_predictor(image, detection).parts()]
            yield face_landmarks


def main():
    import os
    import sys

    # importing-modules-from-parent-folder
    sys.path.insert(0,'..')

    from helper import download_drive_file
    from settings import MODELS_DIR, MEDIA_DIR

    # https://drive.google.com/file/d/1lHlWxCIhMLhq06EtCA-dGOHTdGqfSiGD/view?usp=sharing
    
    landmarks_model_path = os.path.join(MODELS_DIR, "shape_predictor_68_face_landmarks.dat")
    if not os.path.exists(landmarks_model_path):
        download_drive_file(file_id="1lHlWxCIhMLhq06EtCA-dGOHTdGqfSiGD", output=landmarks_model_path)
    
    landmarks_detector = LandmarksDetector(landmarks_model_path)

    # test
    raw_img_path = os.path.join(MEDIA_DIR, "images/full/c.jpeg")
    landmarks = landmarks_detector.get_landmarks(raw_img_path)

    print(f"landmarks: {landmarks}")
    pass

if __name__ == "__main__":
    main()