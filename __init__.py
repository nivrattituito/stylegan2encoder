# Add stylegan2encoder in sys path
# Purpose:
#    After adding stylegan2encoder repository as submodule in face ageing project
#    we are getting error message such as 
#        File "E:\python\projects\CartoonGAN\utngl-app\face-swapping-ageing\stylegan2encoder\projector.py", line 12, in <module>
#        from training import misc
#        ModuleNotFoundError: No module named 'training'
#    after loading projector.py from root face-swapping-ageing folder. 
# 
#    so to load projector.py from both face-swapping-ageing and stylegan2encoder folder we have added stylegan2encoder in syspath
# result:
#    After adding stylegan2encoder in sys_path
#    1) We are able to load Projector class from parent face-swapping-ageing as
#        from stylegan2encoder.projector import Projector
#    2) And from stylegan2encoder folder as
#        from projector import Projector

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# parentdir = os.path.dirname(currentdir)
sys.path.insert(0, currentdir) 