import os
import math
import pickle
import imageio
import PIL.Image
import numpy as np
from PIL import Image
import tensorflow as tf
import moviepy.editor as mpy
from google.colab import files
import matplotlib.pyplot as plt
from IPython.display import clear_output
from moviepy.video.io.ffmpeg_writer import FFMPEG_VideoWriter


#**Declare the necessary functions**
def plot_two_images(img1,img2, img_id, fs = 12):
  f, axarr = plt.subplots(1,2, figsize=(fs,fs))
  axarr[0].imshow(img1)
  axarr[0].title.set_text('Encoded img %d' %img_id)
  axarr[1].imshow(img2)
  axarr[1].title.set_text('Original img %d' %img_id)
  plt.setp(plt.gcf().get_axes(), xticks=[], yticks=[])
  plt.show()

def display_sbs(folder1, folder2, res = 256):
  if folder1[-1] != '/': folder1 += '/'
  if folder2[-1] != '/': folder2 += '/'
    
  imgs1 = sorted([f for f in os.listdir(folder1) if '.png' in f])
  imgs2 = sorted([f for f in os.listdir(folder2) if '.png' in f])
  if len(imgs1)!=len(imgs2):
    print("Found different amount of images in aligned vs raw image directories. That's not supposed to happen...")
  
  for i in range(len(imgs1)):
    img1 = Image.open(folder1 + imgs1[i]).resize((res,res))
    img2 = Image.open(folder2 + imgs2[i]).resize((res,res))
    plot_two_images(img1,img2, i)
    print("")
     
def move_latent_and_save(latent_vector, direction_file, coeffs, Gs_network, Gs_syn_kwargs):
    # direction = np.load('latent_directions/' + direction_file)
    direction = np.load('latent_directions/' + direction_file)
    os.makedirs('results/'+direction_file.split('.')[0], exist_ok=True)
    
    for i, coeff in enumerate(coeffs):
        new_latent_vector = latent_vector.copy()
        new_latent_vector[0][:8] = (latent_vector[0] + coeff*direction)[:8]
        images = Gs_network.components.synthesis.run(new_latent_vector, **Gs_syn_kwargs)
        result = PIL.Image.fromarray(images[0], 'RGB')
        result.thumbnail(size, PIL.Image.ANTIALIAS)
        result.save('results/'+direction_file.split('.')[0]+'/'+str(i).zfill(3)+'.png')
        if len(coeffs)==1:
          return result


# configure generator
def configure_generator(network='default', model=None, other_networks=[], networks_urls={}):
    """
    Configure stylegan generator

    Args:
        network (str, optional): Stylegan network. Defaults to 'default'. network = 'default' #@param ["default", "european", "asian", "asian beauty", "baby"]
        model (str, optional): Stylegan2 model name if network default is selected. Defaults to None.
        other_networks (list, optional): list of network names. Defaults to [].
        networks_urls (dict, optional): dict of . Defaults to {}.
    """
    if network == 'default':
        network_pkl = 'networks/' + model
    else:
        # other_networks = !ls networks/other
        if networks_urls[network][1] in other_networks:
            network_pkl = 'networks/other/' + networks_urls[network][1]
        else:
            network_url = networks_urls[network][0]
            try:
                logger.info(f"downloading file from google drive. Path: {network_url}")
                output_folder = os.path.join('networks/other/', network_name)
                download(network_url, output_folder, quiet=False)

                network_name = networks_urls[network][1]
                network_pkl = 'networks/other/' + network_name
            except BaseException:
                network_pkl = 'networks/other/' + networks_urls[network][1]

    tflib.init_tf()
    with open(network_pkl, "rb") as f:
        generator_network, discriminator_network, Gs_network = pickle.load(f)

    w_avg = Gs_network.get_var('dlatent_avg')
    noise_vars = [var for name, var in Gs_network.components.synthesis.vars.items() if name.startswith('noise')]
    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = 1
    truncation_psi = 0.5


def generate_photo_animation(latent_file, parameter, intensity, boost_intensity=False, resolution="512"):
    """
    generate photo animation by modifying single feature

    Args:
        latent_file (str): Latent vector file
        parameter (str): feature to modify. ex. ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
        intensity (float): value to modify feature
        boost_intensity (bool, optional): If you set the boost_intensity, then the intensity will increase 3 times.. Defaults to False.
        resolution (str, optional): Resolution. Defaults to "512". values are [128, 256, 512, 1024]
    """
    from edit_photo import move_latent_and_save
    v = np.load(latent_file)
    v = np.array([v])

    direction_file = parameter + '.npy'
    direction_name = direction_file.split('.')[0]

    if boost_intensity == True:
	    intensity *= 3
	    coeffs = [intensity]

    size = int(resolution), int(resolution)

    move_latent_and_save(
        v, direction_file, coeffs, Gs_network, Gs_syn_kwargs
    )